--[[
	Mod Gestor para Minetest
	Gestor v1.0 Copyright (C) 2016 BrunoMine (https://github.com/BrunoMine)
	
	Recebeste uma cópia da GNU Lesser General
	Public License junto com esse software,
	se não, veja em <http://www.gnu.org/licenses/>. 
	
	Diretrizes
  ]]

-- Variavel de Diretrizes
gestor.diretrizes = {}

-- Estruturas
gestor.diretrizes.estruturas = {
	--	arquivo,			largura,	altura
		-- Centro
		["centro"] = 	{	10,		10	},
		-- Vilas
}

-- Lista de vilas (lista de estruturas ja salvas)
gestor.vilas = {
	-- "exemplo",
}



